﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour {

    public Transform groundHolder;
    public Transform turretHolder;
    //new
    public Transform powerupHolder;

    public enum MapDataFormat {     //enumeration: variable in a set of constants
        Base64,                     //the .XML file attached to the tileset image is encoded in Base64
        CSV                         //Comma-separated values
    }

    public MapDataFormat mapDataFormat;
    public Texture2D spriteSheetTexture;

    public GameObject tilePrefab;
    public GameObject turretPrefab;
    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<Vector3> turretPositions;
    //new
    public GameObject powerupPrefab;
    public List<Vector3> powerupPositions;

    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    public string TMXFilename;

    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;

    public int pixelsPerUnit = 32;  //size of tile, a value used to determine tileWidth and tileHeight

    public int tileWidthInPixels;   //this value is later being read off of the XML document
    public int tileHeightInPixels;  //""
    float tileWidth;
    float tileHeight;

    public int spriteSheetColumns;  
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    public string mapDataString;
    public List<int> mapData;




    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    static void ClearEditorConsole() {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditorInternal.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }


    static void DestroyChildren(Transform parent) {
        for (int i = parent.childCount - 1; i >= 0; i--) {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET

    public void LoadLevel() {

        ClearEditorConsole();
        DestroyChildren(groundHolder);
        DestroyChildren(turretHolder);
        //new
        DestroyChildren(powerupHolder);

        //  mapsDirectory (string) leads to the "Maps" folder within the StreamingAssets folder
        //  TMXFile (string) leads to TMXFilename in mapsDirectory (StreamingAssets->Maps)
        //  TMXFilename is indicated in the Inspector for LevelManager (in the scene)

        /* UNITY API    :   Streaming Assets (Application.streamingAssetsPath)
           Files placed in this folder will be copied to the target machine.
           Place files in here to have them load during game play, and not prior(?) */

        /*  UNITY API   :   Path.Combine(string, string)
            Concatenates (link things together in a chain) two path strings. */
        {
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        // mapData dicates what tile should go where; reads off XML

        /* FROM https://www.studica.com/blog/read-xml-file-in-unity
           Element  :   tag in XML file. 
                    ie. <Element> </Element>
           Attribute    :   variables assigned to values within Element.
                    ie. <Element SIZE = "1">, in this case "SIZE" is the Attribute of the Element. */

        /*  FROM https://msdn.microsoft.com/en-us/library/sf1aw27b(v=vs.110).aspx
            Convert.ToInt32 :   Converts the specified string representation of 
                                a number to an equivalent 32-bit signed integer.
                    Basically, convering a string found in the XML file into
                    an int readable/usable in C#.  */
        {
            mapData.Clear();    //Clearing mapData

            string content = File.ReadAllText(TMXFile);

            using (XmlReader reader = XmlReader.Create(new StringReader(content))) {

                //  Element "map", grabbing the Attributes "width" and "height"
                //  and assigning them to mapColumns and mapRows.
                //  "width" and "height" measures the size of the map created in Tiled and used in-game
                //  It is 20 x 15 tiles large.
                reader.ReadToFollowing("map");
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));

                //  THIS IS THE SPRITE SHEET USED TO FORMULATE THE MAP
                //  Element "tileset", grabbing the Attributes "tilewidth" and "tileheight"
                //  and assigning them to tileWidthInPixels and tileHeightInPixels.
                //  The size of each tile in pixels is 32 x 32.
                reader.ReadToFollowing("tileset");
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));

                //  Assigning Attribute "tilecount" to local variable spriteSheetTileCount (48)
                //  Assigning Attribute "columns" to spriteSheetColumns (8)
                //  Total number of tiles used in the SPRITE SHEET = 48
                //  Number of Columns in SPRITE SHEET = 8
                //  spriteSheetRows is 48 / 8 = 6
                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;

                //  Element "image", grabbing the "source"
                //  which is then getting the source image by combining strings (like how we got the TMX file
                //  via StreamingAssets) and since "source" is "desert.png", it obtains that file from mapsDirectory
                reader.ReadToFollowing("image");
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));

                //  The Element "data" is within the Element "Layer"

                /*  Switch/Case :   Switching statements
                            In this case, if (encodingType) is case "base64", use Base64 Map Data Format
                            and if it's case "csv", switch to CSV   */

                //  In this particular project, the .TMX file has "base64" as its encoding type.
                //  Later, we dictate specific ways to read Base64  and CSV files, respectively.

                reader.ReadToFollowing("layer");
                reader.ReadToFollowing("data");
                string encodingType = reader.GetAttribute("encoding");

                switch (encodingType) {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }

                //  Reads the last listed Element (or "current Element") as a string, and trims it off white-space characters
                //  HgAAAA...   <-  this stuff
                mapDataString = reader.ReadElementContentAsString().Trim();

                //OBJECT positions in the TMX, which are the TURRET POSITIONS
                turretPositions.Clear();    //clearing turret positions

                /*  FROM https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/do
                    Do statement    :   executes a statement REPEATEDLY until a specified expression evaluates to FALSE.
                    While statement :   executes a statement until a specified expression evaluates to FALSE.
                                        Checks for FALSE at the beginning of a loop.
                    >Do-while loop   :   executed one time before conditional expression is evaluated.
                                        Checks for FALSE at the end of a loop (it is an exit condition loop).  */

                /*  FROM https://msdn.microsoft.com/en-us/library/system.convert.tosingle(v=vs.110).aspx
                    Convert.ToSingle(string)    :   Converts a specified string representation of a number
                                                    to a single-precision floating-point number. */

                //  Obtaining the "x" and "y" (positions) of the Objects, dividing it by 32px (per tile)
                //  Instantiating a turret at that x, y
                //  Doing everything in "Do" and then afterwards, "While" moves onto the next Object
                if (reader.ReadToFollowing("objectgroup")) {
                    if (reader.ReadToDescendant("object")) {
                        do {
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            turretPositions.Add(new Vector3(x, -y, 0));

                        } while (reader.ReadToNextSibling("object"));
                    }
                }

                //new
                powerupPositions.Clear();
                //new
                if (reader.ReadToFollowing("objectgroup"))
                {
                    if (reader.ReadToDescendant("object"))
                    {
                        do
                        {
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            powerupPositions.Add(new Vector3(x, -y, 0));

                        } while (reader.ReadToNextSibling("object"));
                    }
                }

            }

            //  Converting the data of Base64/CSV into something usable, into int
            //  The -1 in the tileID is due to the fact that TMX starts the array at 1, while Unity starts it at 0.
            switch (mapDataFormat) {

                case MapDataFormat.Base64:

                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    int index = 0;
                    while (index < bytes.Length) {
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        mapData.Add(tileID);
                        index += 4;
                    }
                    break;


                case MapDataFormat.CSV:

                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    foreach (string line in lines) {
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (string value in values) {
                            int tileID = Convert.ToInt32(value) - 1;
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }


        {
            //  Obtains the size of each tile
            //  32 / 32 = 1
            //  Counting each tile as 1 unit
            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);

            //  Because the default anchor point of images/each tile is at 0,0 (top-left corner), offsetting it
            //  by half its size (1) moves the anchor point of each time to the center, or 0.5,0.5.
            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




        //  BASICALLY creating a 2D Texture FROM the sprite sheet (which we said the location of above)
        //  Changing its Filter Mode and Wrap Mode, just like you would in the editor
        //  But this is through code :c
        {
            spriteSheetTexture = new Texture2D(2, 2);
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            spriteSheetTexture.filterMode = FilterMode.Point;
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        // Creating separate sprites out from the sprite sheet
        //  As you would in the editor (Slicing)
        {
            mapSprites.Clear(); //clearing mapSprites

            for (int y = spriteSheetRows - 1; y >= 0; y--) {
                for (int x = 0; x < spriteSheetColumns; x++) {
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    mapSprites.Add(newSprite);
                }
            }
        }

        //  Instantiating prefabs into the scene
        //  1. Instantiates an empty/blank prefab
        //  2. Uses tileID to assign the correct sprite tile
        //  3. Adds tile to the prefab
        {
            tiles.Clear();

            for (int y = 0; y < mapRows; y++) {
                for (int x = 0; x < mapColumns; x++) {

                    //  It obtains the tileID from the map data
                    //  It uses mapColumns because it changes lines once it reaches the end
                    //  ie. 0   1   2   3   4   5   6   7
                    //    > 8   9   10  11  12  13  14  15      etc.
                    int mapDatatIndex = x + (y * mapColumns);
                    int tileID = mapData[mapDatatIndex];

                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                    tile.transform.parent = groundHolder;
                    tiles.Add(tile);
                }
            }
        }


        // Instantiating a Turret prefab at "turretPosition", dictated earlier
        {
            foreach (Vector3 turretPosition in turretPositions) {
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                turret.name = "Turret";
                turret.transform.parent = turretHolder;
            }
        }

        //new
        {
            foreach (Vector3 powerupPosition in powerupPositions)
            {
                GameObject powerUp = Instantiate(powerupPrefab, powerupPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                powerUp.name = "PowerUp";
                powerUp.transform.parent = powerupHolder;
            }
        }


        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}


