﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{

    public GameObject tank;
    public Tank player;
    public float newSpeed;
    public float originalSpeed;
    public float timer = 3.0f;

    public bool hasPickedUp;

    public SpriteRenderer sprite;
    //public Color colorChange = Color.white;
    public Color colorA;
    public Color colorB;

    // Use this for initialization
    void Start()
    {
        tank = GameObject.Find("Tank");
        player = tank.GetComponent<Tank>();

        //sprite = this.GetComponent<SpriteRenderer>();
        sprite = GetComponentInChildren<SpriteRenderer>();

        colorA = new Color(1f, 0f, 0f, 1f);
        colorB = new Color(0f, 1f, 0f, 1f);

        originalSpeed = player.speed;   //original speed of tank is the variable used in the Tank script--1.5
        newSpeed = originalSpeed + 5;    //newSpeed is Tanks's original speed, +5 (6.5)
        hasPickedUp = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (sprite==null)
        {
            sprite = GetComponentInChildren<SpriteRenderer>();
        }

        if(!player)
        {
            player = GameObject.Find("Tank").GetComponent<Tank>();
        }

        if(hasPickedUp == true)
        {
            timer -= Time.deltaTime;
            if (timer <= 0f)
            {
                timer = 3.0f;
                hasPickedUp = false;
                player.speed = originalSpeed;
            }
        }

        //color lerp
        sprite.color = Color.Lerp(colorA, colorB, Mathf.PingPong(Time.time, 1));

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            PickUp();
        }
    }

    void PickUp()
    {
        //Debug.Log(timer);
        hasPickedUp = true;
        player.speed = newSpeed;
        Debug.Log(player.speed);

    }
}
