﻿using System;
using UnityEngine;

public class PowerUpSave : Save
{
    public Data data;
    private PowerUp powerUp;
    private string jsonString;
    public SpriteRenderer sprite;

    [Serializable]
    public class Data : BaseData
    {
        public Vector3 position;
        public float timer;
        public bool hasPickedUp;
        public float colorR;
        public float colorG;
    }

    void Awake()
    {
        powerUp = GetComponent<PowerUp>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        data = new Data();
    }

    public override string Serialize()
    {
        data.prefabName = prefabName;
        data.position = powerUp.transform.position;
        data.timer = powerUp.timer;
        data.hasPickedUp = powerUp.hasPickedUp;
        data.colorR = powerUp.sprite.color.r;
        data.colorG = powerUp.sprite.color.g;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);  
    }

    public override void Deserialize(string jsonData)
    {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        powerUp.transform.position = data.position;
        powerUp.timer = data.timer;
        powerUp.hasPickedUp = data.hasPickedUp;
        powerUp.GetComponentInChildren<SpriteRenderer>().color = new Color(data.colorR, data.colorG, 0f, 1);
        powerUp.name = "Powerup";
    }
}
