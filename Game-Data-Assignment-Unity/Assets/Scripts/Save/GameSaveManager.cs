﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

//  FROM: https://docs.unity3d.com/ScriptReference/Serializable.html
//  Displays variables in the inspector, much like making a variable public(?)
//  FROM: https://docs.unity3d.com/Manual/JSONSerialization.html
//  Marking a C# class as Serializable is rquired to work with the JSON serializer.
//  FROM: https://gamedevelopment.tutsplus.com/tutorials/how-to-save-and-load-your-players-progress-in-unity--cms-20934
//  Serializing data is important for saving and restoring data.

[Serializable]
public class GameSaveManager : MonoBehaviour {

    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    public List<string> saveItems;
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    public static GameSaveManager Instance { get; private set; }

    //  FROM: http://wiki.unity3d.com/index.php/Singleton
    //  Singletons are design patterns that restricts the Instantiation of a class to one object.
    //  (Usually used for) implementing Global Variables.
    //  FROM: http://clearcutgames.net/home/?p=437
    //  Allows writing a Class that can only be instantiated once. It also doesn't get destroyed between different scenes,
    //  and DOESN'T GET RESET BETWEEN PLAY SESSIONS.

    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake() {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this) {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

        //  FROM: https://docs.unity3d.com/ScriptReference/Application-persistentDataPath.html
        //  Contains the path to a persistent data directory.
        //  >   AppData/LocalLow/SheridanGameDesign/Game Data Assignment/game-save.json
        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        //Debug.Log(gameDataFilePath);


        saveItems = new List<string>();
    }


    public void AddObject(string item) {
        saveItems.Add(item);
    }

    public void Save() {
        saveItems.Clear();  //clear saved items, so things are saved over

        //  An Array ("Savable Objects")
        //  Looks through objects to find objects with type of "Save" (ie. public class TankSave : Save) and places them in the array
        //  ForEach loop: for every object in the savableObjects array, add it to the List called saveItems
        
        //  ** Serialize is also a string in TankSave (and other save scripts). 
        //  Gets string produced by savableObject and puts it in a list of strings
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            saveItems.Add(saveableObject.Serialize());
        }

        //  FROM: https://support.microsoft.com/en-ca/help/304430/how-to-do-basic-file-i-o-in-visual-c
        //  StreamWriter class creates and writes to a file.

        //  Creating a new file, and writing to it
        //  For each item in the string "saveItems", write the item down
        //  file name and path was designated earlier:
        //  >   gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath)) {
            foreach (string item in saveItems) {
                gameDataFileStream.WriteLine(item);
            }
        }
    }

    //  The firstPlay bool is [true] at start, but becomes false once the game is loaded (pressing the load button)
    //  After loading, save clear cache
    //  Loads current scene
    public void Load() {
        firstPlay = false;
        saveItems.Clear();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //  if bool firstPlay is true, stop OnSceneLoaded function right away
    //  Otherwise (firstPlay = false), 
    //  Loads save data, destroys previous objects, and then recreates based off of save data
    //  returns VOID
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (firstPlay) return;

        LoadSaveGameData();
        DestroyAllSaveableObjectsInScene();
        CreateGameObjects();
    }

    //  FROM: https://msdn.microsoft.com/en-us/library/system.io.streamreader.peek(v=vs.110).aspx
    //  Peek: integer representing the next character to be read. Used to determine whether it is the end of the file, or if another error has occurred.
    //      :   -1 if there are no characters to be read.
    //  If peek is positive (>=0), that means it has not reached the end of a line, and if the line is greater than 0
    //  MEANING: if there's /something/ in the line, adfd it to the saveItems list.
    void LoadSaveGameData() {
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath)) {
            while (gameDataFileStream.Peek() >= 0) {
                string line = gameDataFileStream.ReadLine().Trim();
                if (line.Length > 0) {
                    saveItems.Add(line);
                }
            }
        }
    }

    //  An Array ("Savable Objects")
    //  Looks through objects to find objects with type of "Save" (ie. public class TankSave : Save) and places them in the array
    //  ForEach loop: for every object in the savableObjects array, destroy it
    void DestroyAllSaveableObjectsInScene() {
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            Destroy(saveableObject.gameObject);
        }
    }

    //  Look through json
    //  for each item in item in saveItems
    //  finding the start and end index of @"""prefabName"":"""???
    //  FROM: https://stackoverflow.com/questions/556133/whats-the-in-front-of-a-string-in-c
    //  @ gets rid of the /
    //  FROM: Andrew Carvalho
    //  Pulls prefab name from serialized data, so it can instantiate prefab using Resources.Load
    //  Regular Expression/regex: general string search framework
    //  @ symbol lets the compiler know that it's a regex pattern that will be used to search another piece of text.
    void CreateGameObjects() {
        foreach (string saveItem in saveItems) {
            string pattern = @"""prefabName"":""";
            int patternIndex = saveItem.IndexOf(pattern);
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

            //  instantiate item
            //  run the function "Deserialized" of the gameObject item
            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
            item.SendMessage("Deserialize", saveItem);
        }
    }

    //json
    //{"prefabName":"Turret","position":{"x":-1.5625,"y":4.25,"z":0.0},"barrelEulerAngles":{"x":0.0,"y":0.0,"z":0.0},"previousFiringTime":0.0}
}
