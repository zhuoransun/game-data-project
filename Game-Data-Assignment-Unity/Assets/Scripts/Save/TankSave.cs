﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save
{
    public Data data;
    private Tank tank;
    private string jsonString;

    [Serializable]
    public class Data : BaseData
    {
        //  These are all "data" required for the tank;
        //  Its position, its rotation, as well as its direction (destination)
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;

        //added tank speed
        public float speed;
    }

    void Awake()
    {
        tank = GetComponent<Tank>();
        data = new Data();
    }

    //  FROM: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/override
    //  FROM: https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/knowing-when-to-use-override-and-new-keywords
    //  override is required to extend or modify abstract or virtual implementation of an inherited method, property, indexer, or event.
    //  Where one class inherits from the base class(?)

    //  Serialize's data is added to the saveItems string in the GameSaveManager.
    public override string Serialize()
    {
        data.prefabName = prefabName;
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;
        //new
        data.speed = tank.speed;

        //  FROM: https://docs.unity3d.com/ScriptReference/JsonUtility.FromJson.html
        //  Creat an object from its JSON representation.
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);    //  can't return nothing, have to return type 'string'
    }

    //  Has listener in GameSaveManager
    //  FROM: https://docs.unity3d.com/ScriptReference/JsonUtility.FromJsonOverwrite.html
    //  Overwrites data in an object by reading from its JSON representation
    //  Similar to FromJson, except instead of creating new objects and loading JSON data, it loads JSON data into existing objects.
    //  Injects pre-existing JSON data into existing objects!
    public override void Deserialize(string jsonData)
    {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        //new
        tank.speed = data.speed;

        tank.name = "Tank";
    }
}